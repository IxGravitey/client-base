package me.lpk.client.module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

import org.lwjgl.input.Keyboard;
import me.lpk.client.Client;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.management.AbstractManager;
import me.lpk.client.management.SubFolder;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.impl.combat.*;
import me.lpk.client.module.impl.render.*;
import me.lpk.client.module.impl.hud.*;
import me.lpk.client.module.impl.movement.*;
import me.lpk.client.module.impl.other.*;
import me.lpk.client.module.impl.player.*;
import me.lpk.client.module.impl.world.*;
import me.lpk.client.module.impl.world.breaker.*;
import me.lpk.client.module.impl.world.farm.*;

public class ModuleManager<E extends Module> extends AbstractManager<Module> {
	private boolean setup;

	public ModuleManager(Class<Module> clazz) {
		super(clazz, 0);
	}

	/**
	 * Sets up the ModuleManager.
	 * <hr>
	 * Two ways to initiate modules internally:<br>
	 * Modify constructor: so that it looks like: <i>super(clazz, numOfMods)</i>
	 * ; Initiate modules by array index like so:
	 * 
	 * <pre>
	 * array[0] = new ModuleExample(...);
	 * array[1] = new ModuleExample(...);
	 * array[2] = new ModuleExample(...);
	 * </pre>
	 * 
	 * or use the add method (Slight addition to startup time)
	 * 
	 * <pre>
	 *                                 
	 * add(new ModuleExample(...);             
	 * add(new ModuleExample(...);               
	 * add(new ModuleExample(...);
	 * </pre>
	 */
	@Override
	public void setup() {
		// Load modules from jars
		// loadLocalPlugins();
		//
		add(new Climb(new ModuleData(ModuleData.Type.Movement, "Climb", "Climbs walls.", Keyboard.KEY_C, KeyMask.None)));
		add(new Sneak(new ModuleData(ModuleData.Type.Movement, "Sneak", "Sneaks", Keyboard.KEY_Z, KeyMask.None)));
		add(new Sprint(new ModuleData(ModuleData.Type.Movement, "Sprint", "Automatically sprints for you.", Keyboard.KEY_LCONTROL, KeyMask.None)));
		add(new Fly(new ModuleData(ModuleData.Type.Movement, "Fly", "Lets the player fly.", Keyboard.KEY_F, KeyMask.None)));
		add(new SpectatorESP(new ModuleData(ModuleData.Type.Render, "SpectatorESP", "Renders entities like spectators.")));
		add(new BoxESP(new ModuleData(ModuleData.Type.Render, "BoxESP", "Draws boxes around entities.")));
		add(new Xray(new ModuleData(ModuleData.Type.Render, "Xray", "Automatically sprints for you.", Keyboard.KEY_X, KeyMask.None)));
		add(new Fastmine(new ModuleData(ModuleData.Type.World, "Fastmine", "Breaks blocks quicker.", Keyboard.KEY_V, KeyMask.None)));
		add(new AutoTool(new ModuleData(ModuleData.Type.World, "AutoTool", "Selects the best tool for the block over the mouse.", Keyboard.KEY_V, KeyMask.None)));
		add(new AutoWeapon(new ModuleData(ModuleData.Type.Combat, "AutoWeapon", "Selects the best weapon.", Keyboard.KEY_R, KeyMask.None)));
		add(new AntiVelocity(new ModuleData(ModuleData.Type.Combat, "AntiVelocity", "Prevents knockback.", Keyboard.CHAR_NONE, KeyMask.None)));
		add(new LiquidInteract(new ModuleData(ModuleData.Type.World, "LiquidInteract", "Place blocks on water.", Keyboard.CHAR_NONE, KeyMask.None)));
		add(new MultiPlace(new ModuleData(ModuleData.Type.World, "MultiPlace", "Places the item/block multiple times.", Keyboard.CHAR_NONE, KeyMask.None)));
		add(new ChatCommands(new ModuleData(ModuleData.Type.Other, "ChatCommands", "Chat command handler.")));
		add(new Help(new ModuleData(ModuleData.Type.HUD, "HelpUI", "Clickable help UI", Keyboard.KEY_LSHIFT, KeyMask.Control)));
		add(new Enabled(new ModuleData(ModuleData.Type.HUD, "Enabled", "Lists enabled modules.", Keyboard.KEY_LSHIFT, KeyMask.Control)));
		add(new ClickUI(new ModuleData(ModuleData.Type.HUD, "ClickUI", "Click gui", Keyboard.KEY_RSHIFT, KeyMask.None)));
		setup = true;
		//
		get(Enabled.class).toggle();
		get(ChatCommands.class).toggle();
	}

	private void loadLocalPlugins() {
		// Get the directory of the jars
		String basePath = Client.getDataDir().getAbsolutePath();
		String newPath = basePath + ((basePath.endsWith(File.separator)) ? SubFolder.ModuleJars.getFolderName() : File.separator + SubFolder.ModuleJars.getFolderName());
		File test = new File(newPath);
		// Make the directory if it does not exist
		if (!test.exists()) {
			test.mkdirs();
		}
		// Loop through files in the directory
		for (File file : test.listFiles()) {
			// Load jars
			if (file.getAbsolutePath().endsWith(".jar")) {
				try {
					loadJar(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Loads modules from a jar file.
	 * 
	 * @param file
	 * @throws IOException
	 */
	private void loadJar(File file) throws IOException {
		JarInputStream jis = new JarInputStream(new FileInputStream(file));
		URLClassLoader urlLoader = URLClassLoader.newInstance(new URL[] { file.toURI().toURL() });
		for (JarEntry jarEntry = jis.getNextJarEntry(); jarEntry != null; jarEntry = jis.getNextJarEntry()) {
			// Skip non-jar entries
			if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")) {
				continue;
			}
			String className = jarEntry.getName().replace('/', '.').substring(0, jarEntry.getName().length() - ".class".length());
			// Skip internal classes / others
			if (className.contains("$")) {
				continue;
			}
			try {
				// Attempt to load the class and create an instance.
				Class<?> classs = urlLoader.loadClass(className);
				// If the class is a module, load it.
				if (Module.class.isAssignableFrom(classs)) {
					add((Module) classs.newInstance());
				}
			} catch (ReflectiveOperationException e) {
				e.printStackTrace();
			}
		}
		// Close resources when complete.
		jis.close();
		urlLoader.close();
	}

	public boolean isSetup() {
		return setup;
	}

	public boolean isEnabled(Class<? extends Module> clazz) {
		Module module = get(clazz);
		return (module == null) ? false : module.isEnabled();
	}

	public Module get(String name) {
		for (Module module : getArray()) {
			if (module.getName().toLowerCase().equals(name.toLowerCase())){
				return module;
			}
		}
		return null;
	}
}
