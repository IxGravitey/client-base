package me.lpk.client.module.impl.player;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventPacket;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.NetUtil;
import me.lpk.client.util.misc.Timer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

public class MultiPlace extends Module {
	private static final String KEY_TIMES = "ADDITIONAL-TIMES-PLACED";

	public MultiPlace(ModuleData data) {
		super(data);
		settings.put(MultiPlace.KEY_TIMES, new Setting(MultiPlace.KEY_TIMES, 4));
	}

	@Override
	@RegisterEvent(events = { EventPacket.class })
	public void onEvent(Event event) {
		EventPacket ep = (EventPacket) event;
		//Disregard incoming packets
		if (ep.isIncoming()) {
			return;
		}
		//If the packet is a placement packet...
		if (ep.getPacket() instanceof C08PacketPlayerBlockPlacement) {
			//Loop the amount of times it needs to be duplicated
			int times = ((Number) settings.get(KEY_TIMES).getValue()).intValue();
			for (int i = 0; i < times; i++) {
				NetUtil.sendPacketNoEvents(ep.getPacket());
			}
		}
	}
}
