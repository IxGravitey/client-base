package me.lpk.client.module.impl.render;

import org.lwjgl.opengl.GL11;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventRender3D;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;

public class BoxESP extends Module {

	public BoxESP(ModuleData data) {
		super(data);
	}

	@Override
	@RegisterEvent(events = { EventRender3D.class })
	public void onEvent(Event event) {
		EventRender3D eventRender = (EventRender3D) event;
		int i = 0;
		// Pure GL will not be redered at the right XYZ. Offsetting by -1 will
		// fix this. However, more work (thank I can be fucked to try) will be
		// needed to implement rotation.
		eventRender.offset(-1);
		// Example color
		int color = 0;
		color |= 0 << 16;// Red
		color |= 200 << 8;// Blue
		color |= 200; // green
		GL11.glLineWidth(1.0F);
		for (Object o : mc.theWorld.getLoadedEntityList()) {
			if (o == null || o == mc.thePlayer) {
				continue;
			}
			Entity entity = (Entity) o;
			GlStateManager.disableDepth();
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			GlStateManager.color(1f, 1f, 1f, 1f);
			AxisAlignedBB box = AxisAlignedBB.fromBounds(entity.posX - entity.width / 2, entity.posY, entity.posZ - entity.width / 2, entity.posX + entity.width / 2, entity.posY + entity.height, entity.posZ + entity.width / 2);
			GL11.glEnable(GL11.GL_LINE_SMOOTH);
			RenderGlobal.drawOutlinedBoundingBox(box, color);
			GL11.glDisable(GL11.GL_LINE_SMOOTH);
			GlStateManager.enableDepth();
		}
		eventRender.offset(1);
	}
}
