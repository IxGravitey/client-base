package me.lpk.client.module.impl.render;

import java.util.ArrayList;
import java.util.HashSet;

import org.lwjgl.input.Keyboard;

import com.google.common.collect.Lists;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.PlayerUtil;

public class Xray extends Module {
	private static final String KEY_IDS = "BLOCK-IDS";
	private static final String KEY_OPACITY = "WORLD-OPACITY";
	private static final HashSet<Integer> blockIDs = new HashSet<Integer>();
	private int opacity = 160;

	public Xray(ModuleData data) {
		super(data);
		settings.put(Xray.KEY_IDS, new Setting(Xray.KEY_IDS, Lists.newArrayList(10, 11, 8, 9, 14, 15, 16, 21, 41, 42, 46, 48, 52, 56, 57, 61, 62, 73, 74, 84,89, 103, 116, 117, 118, 120,129, 133, 137,145, 152, 153, 154)));
		settings.put(Xray.KEY_OPACITY, new Setting(Xray.KEY_OPACITY, 160));
	}

	@Override
	public void onEnable() {
		blockIDs.clear();
		opacity = ((Number) settings.get(KEY_OPACITY).getValue()).intValue();
		try {
			ArrayList blockList = (ArrayList) settings.get(KEY_IDS).getValue();
			for (Object o : blockList) {
				blockIDs.add(((Number) o).intValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mc.renderGlobal.loadRenderers();
	}

	@Override
	public void onDisable() {
		mc.renderGlobal.loadRenderers();
	}

	@Override
	@RegisterEvent(events = { EventTick.class })
	public void onEvent(Event event) {

	}

	public boolean containsID(int id) {
		return blockIDs.contains(id);
	}

	public int getOpacity() {
		return opacity;
	}
}
