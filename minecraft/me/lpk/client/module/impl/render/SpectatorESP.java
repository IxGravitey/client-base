package me.lpk.client.module.impl.render;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventRender3D;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import net.minecraft.client.settings.KeyBinding;

public class SpectatorESP extends Module {

	public SpectatorESP(ModuleData data) {
		super(data);
	}

	@Override
	public void onDisable() {
		KeyBinding.setKeyBindState(mc.gameSettings.keybindSpectateView.getKeyCode(), false);
	}

	@Override
	@RegisterEvent(events = { EventRender3D.class })
	public void onEvent(Event event) {
		EventRender3D eventRender = (EventRender3D) event;
		/*
		if (eventRender.getX() > 0) {
			eventRender.offset(-1);
		}
		if (eventRender.getX() < 0) {
			eventRender.offset(1);
		}*/
		//TODO: Test if works or is even required
		if (eventRender.isOffset()){
			eventRender.reset();
		}
		KeyBinding.setKeyBindState(mc.gameSettings.keybindSpectateView.getKeyCode(), true);
	}
}
