package me.lpk.client.module.impl.movement;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventMotion;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.NetUtil;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.network.play.client.C0BPacketEntityAction;

public class Sneak extends Module {
	private static final String KEY_SERVER = "IS-SERVER-SIDE";

	public Sneak(ModuleData data) {
		super(data);
		settings.put(Sneak.KEY_SERVER, new Setting(Sneak.KEY_SERVER, true));
	}

	@Override
	public void onEnable() {
		// Enable sneak for the current mode
		Boolean isServerSide = (Boolean) settings.get(Sneak.KEY_SERVER).getValue();
		if (isServerSide.booleanValue()) {
			NetUtil.sendPacketNoEvents(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
		} else {
			KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
		}
	}

	@Override
	public void onDisable() {
		// Disable sneak for the current mode
		Boolean isServerSide = (Boolean) settings.get(Sneak.KEY_SERVER).getValue();
		if (isServerSide.booleanValue()) {
			NetUtil.sendPacketNoEvents(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
		} else {
			KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), false);
		}
	}

	@Override
	@RegisterEvent(events = { EventTick.class, EventMotion.class })
	public void onEvent(Event event) {
		Boolean isServerSide = (Boolean) settings.get(Sneak.KEY_SERVER).getValue();
		// Normal ticking
		if (event instanceof EventTick) {
			// Only run if the setting is client-side
			if (isServerSide.booleanValue()) {
				return;
			}
			KeyBinding.setKeyBindState(mc.gameSettings.keyBindSneak.getKeyCode(), true);
		} // Motion updates
		else if (event instanceof EventMotion) {
			// Only run if the setting is server-side
			if (!isServerSide.booleanValue()) {
				return;
			}
			// Don't sneak for the premotion, sneak for the post-motion. That's
			// what people see.
			boolean isPre = ((EventMotion) event).isPre();
			if (isPre) {
				NetUtil.sendPacketNoEvents(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
			} else {
				NetUtil.sendPacketNoEvents(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
			}
		}
	}
}
