package me.lpk.client.module.impl.movement;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;

public class Fly extends Module {
	private static final String KEY_SPEED = "SPEED";

	public Fly(ModuleData data) {
		super(data);
		settings.put(Fly.KEY_SPEED, new Setting<Float>(Fly.KEY_SPEED, 0.065F));

	}

	@Override
	@RegisterEvent(events = { EventTick.class })
	public void onEvent(Event event) {
		float speed = ((Number) settings.get(Fly.KEY_SPEED).getValue()).floatValue();
		mc.thePlayer.capabilities.setFlySpeed(speed);
		mc.thePlayer.capabilities.isFlying = true;
	}

	@Override
	public void onDisable() {
		mc.thePlayer.capabilities.setFlySpeed(0.05f);
		mc.thePlayer.capabilities.isFlying = false;
	}
}
