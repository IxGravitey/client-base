package me.lpk.client.module.impl.movement;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.PlayerUtil;

public class Sprint extends Module {
	public Sprint(ModuleData data) {
		super(data);
	}

	@Override
	@RegisterEvent(events = { EventTick.class })
	public void onEvent(Event event) {
		if (canSprint()) {
			mc.thePlayer.setSprinting(true);
		}
	}

	private boolean canSprint() {
		if (mc.thePlayer.isCollidedHorizontally) {
			return false;
		}
		if (!PlayerUtil.isMoving()) {
			return false;
		}
		return true;
	}
}
