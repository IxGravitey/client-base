package me.lpk.client.module.impl.hud;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.gui.screen.impl.overlay.GuiClickOverlay;
import me.lpk.client.gui.screen.impl.overlay.GuiHelpOverlay;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;

public class Help extends Module {
	private GuiHelpOverlay screen;

	public Help(ModuleData data) {
		super(data);
	}

	@Override
	public void onEnable() {
		// A new instance is created each time because the user may have resized
		// their screen causing the last screen to be cumbersome to use
		mc.displayGuiScreen(new GuiHelpOverlay());
		toggle();
	}

	@Override
	public void onEvent(Event event) {}
}
