package me.lpk.client.module;

import java.util.HashMap;

import org.lwjgl.input.Keyboard;

import com.google.gson.annotations.Expose;

import me.lpk.client.Client;
import me.lpk.client.event.EventListener;
import me.lpk.client.event.EventSystem;
import me.lpk.client.gui.clickgui.GUILinked;
import me.lpk.client.keybinding.Bindable;
import me.lpk.client.keybinding.KeyHandler;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.keybinding.Keybind;
import me.lpk.client.management.Saveable;
import me.lpk.client.management.SubFolder;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.data.SettingsMap;
import net.minecraft.client.Minecraft;

public abstract class Module extends Saveable implements EventListener, Bindable, Toggleable, GUILinked {
	protected final Minecraft mc = Minecraft.getMinecraft();
	@Expose
	protected final ModuleData data;
	@Expose
	protected final SettingsMap settings = new SettingsMap();
	private Keybind keybind;
	private boolean enabled;

	public Module(ModuleData data) {
		this.data = data;
		setFolderType(SubFolder.Module);
		// Checks to see if the client should be allowed to register keys.
		// Since GSON used the default constructor for initalization this may be
		// called and a key would be registered. This check prevents that.
		setKeybind(new Keybind(this, data.key, data.mask));
		loadData();
	}

	/**
	 * Handles the toggling of the module
	 */
	@Override
	public void toggle() {
		enabled = !enabled;
		// System.out.println(getClass().getName() + ":" + data.name + ":" +
		// enabled);
		if (enabled) {
			// Load the module's saved information and update the appropriate
			// information.
			loadData();
			// Register the class for events
			EventSystem.register(this);
			onEnable();
		} else {
			// Save module data
			save();
			// Unregister the class from event receiving
			EventSystem.unregister(this);
			onDisable();
		}
	}

	private void loadData() {
		Module module = (Module) load();
		if (module != null) {
			if (module.data != null) {
				data.update(module.data);
			}
			if (module.settings != null) {
				settings.update(module.settings);
			}
			setKeybind(new Keybind(this, data.key, data.mask));
		} else {
			// System.out.println("Load[" + getName() + "] failed!");
		}
	}

	@Override
	public void onEnable() {

	}

	@Override
	public void onDisable() {

	}

	@Override
	public void onBindPress() {
		toggle();
	}

	@Override
	public void onBindRelease() {

	}

	/**
	 * Called when externally from the GUI.
	 */
	@Override
	public void onItemClick() {
		toggle();
	}

	/**
	 * Called when externally from the GUI.
	 */
	@Override
	public void onItemRelease() {

	}

	/**
	 * Sets the current keybind to another.
	 * 
	 * @param newBind
	 */
	@Override
	public void setKeybind(Keybind newBind) {
		if (newBind == null) {
			return;
		}
		// Client init
		if (keybind == null) {
			keybind = newBind;
			KeyHandler.register(keybind);
			return;
		}
		// Not client setup
		boolean sameKey = newBind.getKeyInt() == keybind.getKeyInt();
		boolean sameMask = newBind.getMask() == keybind.getMask();
		if (sameKey && !sameMask) {
			KeyHandler.update(this, keybind, newBind);
		} else if (!sameKey) {
			if (KeyHandler.keyHasBinds(keybind.getKeyInt())) {
				KeyHandler.unregister(this, keybind);
			}
			KeyHandler.register(newBind);
		}
		keybind.update(newBind);
		data.key = keybind.getKeyInt();
		data.mask = keybind.getMask();
		save();
		/*
		 * boolean noBind = newBind.getKeyInt() == Keyboard.CHAR_NONE; boolean
		 * isRegistered = KeyHandler.isRegistered(keybind); if (isRegistered) {
		 * if (noBind) { // Unegister the now-unused keybind
		 * KeyHandler.unregister(keybind); } else { // Update the existing
		 * keybind with new information int curKey = keybind.getKeyInt(); int
		 * newKey = newBind.getKeyInt(); if (curKey == newKey) {
		 * KeyHandler.update(keybind, newBind); } else {
		 * KeyHandler.unregister(keybind); KeyHandler.register(newBind); } }
		 * }else{ KeyHandler.register(newBind); } keybind.update(newBind); // if
		 * (!isRegistered && !noBind) { // Register the new keybind
		 * //KeyHandler.register(keybind); // } if (keybind != null) { data.key
		 * = keybind.getKeyInt(); }
		 */
	}

	/**
	 * 
	 * 
	 * TODO: UN FUCK THE GOD FUCKING KEYBINDS
	 * 
	 * What I want to happen: - Client loads up - Module init - - Checks if
	 * settings exist - - - If so load them and use the information for making
	 * the keybind - - - Else use the default as provided in the constructor
	 * 
	 * Then later on: .bind Module key
	 * 
	 * - Check if the new key and current key match, if so just update the bind
	 * (IE: adding a mask) - - If the keys dont match, unregister the old one,
	 * register the new one
	 * 
	 */

	public Keybind getKeybind() {
		return keybind;
	}

	/**
	 * Adds a setting to the settings map
	 * 
	 * @param key
	 * @param setting
	 * @return Returns if the setting was added
	 */
	public boolean addSetting(String key, Setting setting) {
		if (settings.containsKey(key)) {
			return false;
		} else {
			settings.put(key, setting);
			return true;
		}
	}

	public Setting getSetting(String key) {
		return settings.get(key);
	}

	public SettingsMap getSettings() {
		return settings;
	}

	public String getName() {
		return data.name;
	}

	public String getDescription() {
		return data.description;
	}

	public ModuleData.Type getType() {
		return data.type;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
