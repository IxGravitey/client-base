package me.lpk.client;

import java.io.File;

import me.lpk.client.account.Account;
import me.lpk.client.account.AccountManager;
import me.lpk.client.gui.clickgui.renderer.GuiRenderer;
import me.lpk.client.gui.clickgui.renderer.impl.BasicRenderer;
import me.lpk.client.gui.screen.impl.mainmenu.ClientMainMenu;
import me.lpk.client.module.Module;
import me.lpk.client.module.ModuleManager;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;

public class Client {
	// http://pastebin.com/raw.php?i=9DxSAn7m
	public static Client instance;
	// Client data
	public static final String author = "LordPankake";
	public static final String version = "1.0";
	public static final String clientName = "ClientName";
	// Managers
	private final ModuleManager<Module> moduleManager;
	private final AccountManager<Account> accountManager;
	// Other data
	private File dataDirectory;
	private GuiScreen mainMenu = new ClientMainMenu();
	private boolean isHidden;
	private GuiRenderer guiTheme;

	/**
	 * TODO: - Work on vanilla hidden mode - Work on making the renderers (help
	 * UI vs frame UI) use the same class - Work on MP3 player (See above point)
	 * - Add more themes
	 */

	public Client() {
		Client.instance = this;
		moduleManager = new ModuleManager(Module.class);
		accountManager = new AccountManager(Account.class);
	}
	
	public void setup() {
		dataDirectory = new File(Client.clientName);
		moduleManager.setup();
		accountManager.setup();		
		setTheme(new BasicRenderer());
	}

	public static ModuleManager<Module> getModuleManager() {
		return instance.moduleManager;
	}

	public static AccountManager<Account> getAccountManager() {
		return instance.accountManager;
	}

	public static File getDataDir() {
		return instance.dataDirectory;
	}

	public static GuiScreen getMainMenu() {
		return instance.mainMenu;
	}
	public static  void setTheme(GuiRenderer theme) {
		instance.guiTheme = theme;
		instance.guiTheme.setup();
	}
	public static GuiRenderer getGuiTheme() {
		return instance.guiTheme;
	}

	public static boolean isHidden() {
		return instance.isHidden;
	}

	public static void setHidden(boolean hidden) {
		instance.isHidden = hidden;
		if (hidden) {
			instance.mainMenu = new GuiMainMenu();
		} else {
			instance.mainMenu = new ClientMainMenu();
		}
	}
}
