package me.lpk.client.command;

import java.util.Arrays;

import me.lpk.client.module.impl.other.ChatCommands;
import me.lpk.client.util.ChatUtil;
import net.minecraft.util.EnumChatFormatting;

public abstract class Command implements Fireable {
	private final String[] names;
	private final String description;

	public Command(String[] names, String description) {
		this.names = names;
		this.description = description;
	}

	protected void printDescription() {
		String message = EnumChatFormatting.UNDERLINE + getName() + EnumChatFormatting.RESET + ":" + description;
		ChatUtil.printChat(message);
	}

	protected void printUsage() {
		String message = EnumChatFormatting.UNDERLINE + getName() + EnumChatFormatting.RESET + ":" + getUsage();
		ChatUtil.printChat(message);
	}

	public void register(ChatCommands manager) {
		for (String name : names) {
			manager.addCommand(name.toLowerCase(), this);
		}
	}

	public abstract String getUsage();

	public String getName() {
		return names[0];
	}

	public boolean isMatch(String text) {
		return Arrays.asList(names).contains(text.toLowerCase());
	}

	public String getDescription() {
		return description;
	}
}
