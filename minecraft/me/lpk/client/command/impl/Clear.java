package me.lpk.client.command.impl;

import java.util.ArrayList;

import me.lpk.client.Client;
import me.lpk.client.command.Command;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.data.SettingsMap;
import me.lpk.client.module.impl.other.ChatCommands;
import me.lpk.client.util.ChatUtil;
import me.lpk.client.util.StringConversions;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;

public class Clear extends Command {

	public Clear(String[] names, String description) {
		super(names, description);
	}

	@Override
	public void fire(String[] args) {
		Minecraft.getMinecraft().ingameGUI.getChatGUI().clearChatMessages();
	}

	@Override
	public String getUsage() {
		return "Clear";
	}

}
