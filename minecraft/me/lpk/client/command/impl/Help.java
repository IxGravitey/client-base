package me.lpk.client.command.impl;

import java.util.ArrayList;

import me.lpk.client.Client;
import me.lpk.client.command.Command;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.data.SettingsMap;
import me.lpk.client.module.impl.other.ChatCommands;
import me.lpk.client.util.ChatUtil;
import me.lpk.client.util.StringConversions;
import net.minecraft.util.EnumChatFormatting;

public class Help extends Command {

	public Help(String[] names, String description) {
		super(names, description);
	}

	@Override
	public void fire(String[] args) {
		// Intended arguements
		// 0 - List commands
		// 1 - List details for command
		int i = 1;
		ChatCommands commands = (ChatCommands) Client.getModuleManager().get(ChatCommands.class);
		if (args == null) {
			// Due to commands having being registered multiple times via aliases,
			// take only the first instance (Determined by the names in the alias
			// String[])
			ArrayList<String> used = new ArrayList<String>();
			for (Command command : commands.getCommands()) {
				if (used.contains(command.getName())) {
					continue;
				}
				used.add(command.getName());
				ChatUtil.printChat(i + ". " + command.getName());
				i++;
			}
			ChatUtil.printChat("Specify a name of a command for details about it.");
		} else if (args.length > 0) {
			String name = args[0];
			Command command = commands.getCommand(name);
			if (command == null){
				ChatUtil.printChat("Could not find: " + name);
				return;
			}
			ChatUtil.printChat(command.getName() + ": " + command.getDescription());
			ChatUtil.printChat(command.getUsage());
		}
	}

	@Override
	public String getUsage() {
		return "Help " +EnumChatFormatting.ITALIC + "[optional]" + EnumChatFormatting.RESET+"<Command>";
	}

}
