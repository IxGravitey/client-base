package me.lpk.client.command.impl;

import me.lpk.client.Client;
import me.lpk.client.command.Command;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.data.SettingsMap;
import me.lpk.client.util.ChatUtil;
import me.lpk.client.util.StringConversions;
import net.minecraft.util.EnumChatFormatting;

public class Settings extends Command {

	public Settings(String[] names, String description) {
		super(names, description);
	}

	@Override
	public void fire(String[] args) {
		// Intended arguements
		// 1 - Module
		// 2 - Setting
		// 3 - Value
		if (args == null) {
			printUsage();
			return;
		}
		// Make sure the user inputs a valid Module
		Module module = null;
		if (args.length > 0) {
			module = Client.getModuleManager().get(args[0]);
		}
		if (module == null) {
			printUsage();
			return;
		}
		// .setting Module
		if (args.length == 1) {
			SettingsMap moduleSettings = module.getSettings();
			ChatUtil.printChat("[" + EnumChatFormatting.BOLD + module.getName() + EnumChatFormatting.RESET + "] - Settings:" + moduleSettings.size());
			for (Setting setting : moduleSettings.values()) {
				if (setting != null) {
					printSetting(setting);
				}
			}
		} else if (args.length >= 2) {
			// If there are two or more arguments, get the second arg as a
			// setting
			Setting setting = getSetting(module.getSettings(), args[1]);
			if (setting == null) {
				printUsage();
				return;
			}
			// .setting Module Setting
			if (args.length == 2) {
				// Print the information of the given setting
				printSetting(setting);
			} // .setting Module Setting Value
			else if (args.length >= 3) {
				String objText = args[2];
				try {
					// If the setting is supposed to be numeric
					if (setting.getValue() instanceof Number) {
						Object newValue = (StringConversions.castNumber(objText, setting.getValue()));
						if (newValue != null) {
							setting.setValue(newValue);
							module.save();
							return;
						}
					} // If the setting is supposed to be a string
					else if (setting.getValue().getClass().equals(String.class)) {
						setting.setValue(objText);
						module.save();
						return;
					} //If the setting is supposed to be a boolean
					else if (setting.getValue().getClass().equals(Boolean.class)) {
						setting.setValue(Boolean.parseBoolean(objText));
						module.save();
						return;
					}
					// Non-numeric, non-string, non boolean setting values
					else {
						// Possibly an arraylist or something
					}
				} catch (Exception e) {}
				// Setting could not be applied, therefore print error
				ChatUtil.printChat(EnumChatFormatting.BOLD + "ERROR" + EnumChatFormatting.RESET + ": Could not apply the value '" + objText + "' to " + module.getName() + "'s " + setting.getName());
			}
		}
	}

	/**
	 * Gets the setting with the given name. If a full name is not provided, it
	 * guesses what was intended by checking if the given text is the beginning
	 * of an existing setting's name.
	 * 
	 * @param map
	 * @param settingText
	 * @return
	 */
	private Setting getSetting(SettingsMap map, String settingText) {
		settingText = settingText.toUpperCase();
		if (map.containsKey(settingText)) {
			return map.get(settingText);
		} else {
			for (String key : map.keySet()) {
				if (key.startsWith(settingText)) {
					return map.get(key);
				}
			}
		}
		return null;
	}

	/**
	 * Print out the information of a given Setting.
	 */
	private void printSetting(Setting setting) {
		// Print usage if the setting is not found
		if (setting == null) {
			printUsage();
			return;
		}
		// Get the type as a string
		String typeStr = setting.getType() == null ? setting.getValue().getClass().getSimpleName() : setting.getType().getTypeName();
		if (typeStr.contains(".")) {
			typeStr = typeStr.substring(typeStr.lastIndexOf(".") + 1);
		}
		// Print formatted string with information
		String settingText = EnumChatFormatting.BOLD + setting.getName() + EnumChatFormatting.RESET + ":" + EnumChatFormatting.ITALIC + typeStr + EnumChatFormatting.RESET + ":" + setting.getValue();
		ChatUtil.printChat(settingText);
	}

	@Override
	public String getUsage() {
		return "set <Module> " + EnumChatFormatting.ITALIC + "[optional]" + EnumChatFormatting.RESET + "<Option> " + EnumChatFormatting.ITALIC + "[optional]" + EnumChatFormatting.RESET + "<Value>";
	}

}
