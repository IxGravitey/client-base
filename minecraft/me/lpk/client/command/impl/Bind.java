package me.lpk.client.command.impl;

import org.lwjgl.input.Keyboard;

import me.lpk.client.Client;
import me.lpk.client.command.Command;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.keybinding.Keybind;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.data.SettingsMap;
import me.lpk.client.util.ChatUtil;
import me.lpk.client.util.StringConversions;
import net.minecraft.util.EnumChatFormatting;

public class Bind extends Command {

	public Bind(String[] names, String description) {
		super(names, description);
	}

	@Override
	public void fire(String[] args) {
		// Intended arguements
		// 1 - Module
		// 2 - Key
		// 3 - Mask
		if (args == null) {
			printUsage();
			return;
		}
		// Make sure the user inputs a valid Module
		Module module = null;
		if (args.length > 0) {
			module = Client.getModuleManager().get(args[0]);
		}
		if (module == null) {
			printUsage();
			return;
		}
		if (args.length == 1) {
			Keybind key = module.getKeybind();
			ChatUtil.printChat(EnumChatFormatting.ITALIC + module.getName() + EnumChatFormatting.RESET + ": " + (key.getMask() == KeyMask.None ? "" : key.getMask().name() + " + ") + key.getKeyStr());
		} else if (args.length == 2) {
			int keyIndex = Keyboard.getKeyIndex(args[1].toUpperCase());
			Keybind keybind = new Keybind(module, keyIndex);
			module.setKeybind(keybind);
			Keybind key = module.getKeybind();
			ChatUtil.printChat("Set " + EnumChatFormatting.ITALIC + module.getName() + EnumChatFormatting.RESET + " to " + (key.getMask() == KeyMask.None ? "" : key.getMask().name() + " + ") + key.getKeyStr());
		} else if (args.length == 3) {
			int keyIndex = Keyboard.getKeyIndex(args[1].toUpperCase());
			KeyMask mask = KeyMask.getMask(args[2]);
			Keybind keybind = new Keybind(module, keyIndex, mask);
			module.setKeybind(keybind);
			Keybind key = module.getKeybind();
			ChatUtil.printChat("Set " + EnumChatFormatting.ITALIC + module.getName() + EnumChatFormatting.RESET + " to " + (key.getMask() == KeyMask.None ? "" : key.getMask().name() + " + ") + key.getKeyStr());
		}
	}

	@Override
	public String getUsage() {
		return "bind <Module> " + EnumChatFormatting.ITALIC + "[optional]" + EnumChatFormatting.RESET + "<Key> " + EnumChatFormatting.ITALIC + "[optional]" + EnumChatFormatting.RESET + "<Mask>";
	}

}
