package me.lpk.client.gui.screen.impl.mainmenu;

import com.google.common.collect.Lists;

import me.lpk.client.Client;
import me.lpk.client.gui.screen.PanoramaScreen;
import me.lpk.client.management.SubFolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiButtonLanguage;
import net.minecraft.client.gui.GuiConfirmOpenLink;
import net.minecraft.client.gui.GuiLanguage;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.resources.I18n;
import net.minecraft.realms.RealmsBridge;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.demo.DemoWorldServer;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.WorldInfo;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.Project;

public class ClientMainMenu extends PanoramaScreen {
	private static int key = Keyboard.KEY_GRAVE;
	private static final GuiVanillaMainMenu menuVanilla = new GuiVanillaMainMenu();
	private static final GuiModdedMainMenu menuModded = new GuiModdedMainMenu();

	public void initGui() {
		load();
		if (getClass().equals(ClientMainMenu.class)) {
			display();
		}
	}

	private void load() {
		String file = "";
		try {
			file = FileUtils.readFileToString(getFile());
		} catch (IOException e) {
			return;
		}
		for (String line : file.split("\n")) {
			if (line.contains("key")) {
				String[] split = line.split(":");
				if (split.length > 1) {
					try {
						key = Integer.parseInt(split[1]);
					} catch (NumberFormatException n) {}
				}
			}
		}
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException {
		if (keyCode == key) {
			toggleVanilla();
			display();
		}
	}

	private void display() {
		if (Client.isHidden()) {
			Minecraft.getMinecraft().displayGuiScreen(menuVanilla);
		} else {//menuModded
			Minecraft.getMinecraft().displayGuiScreen(new GuiModdedMainMenu());
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	public void toggleVanilla() {
		Client.setHidden(!Client.isHidden());
		save();
	}

	public void save() {
		try {
			FileUtils.write(getFile(), "Swap key (Toggles menus):" + key);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getFile() {
		File file = new File(getFolder().getAbsolutePath() + File.separator + "MainMenu.txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	public File getFolder() {
		File folder = new File(Client.getDataDir().getAbsolutePath() + File.separator + SubFolder.Other.getFolderName());
		if (!folder.exists()) {
			folder.mkdirs();
		}
		return folder;
	}
}
