package me.lpk.client.gui.screen.impl.mainmenu;

import com.google.common.collect.Lists;

import me.lpk.client.Client;
import me.lpk.client.gui.screen.PanoramaScreen;
import me.lpk.client.gui.screen.component.GuiMenuButton;
import me.lpk.client.gui.screen.impl.GuiAccountList;
import me.lpk.client.management.SubFolder;
import me.lpk.client.util.render.Colors;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiButtonLanguage;
import net.minecraft.client.gui.GuiConfirmOpenLink;
import net.minecraft.client.gui.GuiLanguage;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.resources.I18n;
import net.minecraft.realms.RealmsBridge;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.demo.DemoWorldServer;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.WorldInfo;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.Project;

public class GuiModdedMainMenu extends ClientMainMenu {
	@Override
	public void initGui() {
		super.initGui();
		String strSSP = I18n.format("menu.singleplayer", new Object[0]);
		String strSMP = I18n.format("menu.multiplayer", new Object[0]);
		String strOptions = I18n.format("menu.options", new Object[0]);
		String strQuit = I18n.format("menu.quit", new Object[0]);
		String strLang = I18n.format("Language", new Object[0]);
		String strAccounts = "Accounts";
		int initHeight = this.height / 4 + 48;
		int objHeight = 24 * 2;
		int objWidth = 24 * 4;
		int objPadding = 4;
		int xLeft = width / 2 - objWidth / 2 - objWidth - objPadding;
		int xMid = width / 2 - objWidth / 2;
		int xRight = width / 2 - objWidth / 2 + objWidth + objPadding;
		int yOff = objHeight + objPadding;
		this.buttonList.add(new GuiMenuButton(0, xLeft, initHeight, objWidth, objHeight, strSSP));
		this.buttonList.add(new GuiMenuButton(1, xLeft, initHeight + yOff, objWidth, objHeight, strSMP));
		this.buttonList.add(new GuiMenuButton(2, xMid, initHeight, objWidth, objHeight, strOptions));
		this.buttonList.add(new GuiMenuButton(3, xMid, initHeight + yOff, objWidth, objHeight, strLang));
		this.buttonList.add(new GuiMenuButton(4, xRight, initHeight, objWidth, objHeight, strAccounts));
		this.buttonList.add(new GuiMenuButton(5, xRight, initHeight + yOff, objWidth, objHeight, strQuit));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			mc.displayGuiScreen(new GuiSelectWorld(this));
		} else if (button.id == 1) {
			mc.displayGuiScreen(new GuiMultiplayer(this));
		} else if (button.id == 2) {
			mc.displayGuiScreen(new GuiOptions(this, mc.gameSettings));
		} else if (button.id == 3) {
			mc.displayGuiScreen(new GuiLanguage(this, mc.gameSettings, mc.getLanguageManager()));
		} else if (button.id == 4) {
			mc.displayGuiScreen(new GuiAccountList(this));
		} else if (button.id == 5) {
			mc.shutdown();
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		String str = Client.clientName + " - " + Client.version;
		drawString(fontRendererObj, str, 3, height - 11, Colors.getColor(200));
	}
}
