package me.lpk.client.gui.screen.impl;

import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import me.lpk.client.Client;
import me.lpk.client.account.Account;
import me.lpk.client.account.AccountManager;
import me.lpk.client.account.relation.UserStatus;
import me.lpk.client.gui.screen.PanoramaScreen;
import me.lpk.client.gui.screen.impl.mainmenu.ClientMainMenu;
import me.lpk.client.util.render.Colors;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.PasswordField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Session;

public class GuiAccountList extends PanoramaScreen {
	private final static int SIDEBAR_WIDTH = 130;
	private GuiTextField searchBox;
	private GuiTextField usernameBox;
	private PasswordField passwordBox;
	private final GuiScreen backscreen;
	private static String text;
	private boolean deleteMenuOpen;
	private boolean addingAccount;
	private SlotAlt accountSlot;
	private final boolean isInit;

	public GuiAccountList(GuiScreen backscreen) {
		this.backscreen = backscreen;
		this.isInit = (backscreen instanceof GuiClientInit);
	}

	@Override
	public void initGui() {
		Minecraft mc = Minecraft.getMinecraft();
		Client.getAccountManager().reload();
		int btnHeight = 20;
		int btnY = this.height - btnHeight * 7;
		int btnWidth = 200 / 2 - 2;
		int btnX = 20;
		int i = 0;
		if (!isInit) {
			buttonList.add(new GuiButton(0, btnX, btnY + btnHeight * i++, btnWidth, btnHeight, "Add Account"));
			buttonList.add(new GuiButton(1, btnX, btnY + btnHeight * i++, btnWidth, btnHeight, "Delete Selected"));
		}
		buttonList.add(new GuiButton(2, btnX, btnY + btnHeight * i++, btnWidth, btnHeight, isInit ? "Use Account" : "Login"));
		buttonList.add(new GuiButton(3, btnX, btnY + btnHeight * i++, btnWidth, btnHeight, "Back"));
		Keyboard.enableRepeatEvents(true);
		searchBox = new GuiTextField(6, fontRendererObj, btnX, btnY + btnHeight * (1 + i++), btnWidth, btnHeight);
		searchBox.setMaxStringLength(50);
		accountSlot = new SlotAlt(mc, this);
		accountSlot.registerScrollButtons(7, 8);
		usernameBox = new GuiTextField(6, fontRendererObj, width / 2 - 100, 76, 200, 20);
		passwordBox = new PasswordField(fontRendererObj, width / 2 - 100, 116, 200, 20);
		usernameBox.setMaxStringLength(50);
		passwordBox.setMaxStringLength(50);
		usernameBox.setVisible(false);
		passwordBox.setVisible(false);
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			addingAccount = true;
			text = "";
		} else if (button.id == 1) {
			showDelete();
		} else if (button.id == 2) {
			Account account = Client.getAccountManager().get(accountSlot.getSelected());
			Session session = Session.loginPassword(account.getUser(), account.getPass(), mc.proxy);
			if (session != null) {
				mc.setSession(session);
				account.setDisplay(session.getUsername());
				account.updateCrypto();
				account.save();
				Client.getAccountManager().setCurrent(account);
				if (isInit) {
					mc.displayGuiScreen(new ClientMainMenu());
				}
			}
		} else if (button.id == 3) {
			mc.displayGuiScreen(backscreen);
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		panoramaTimer++;
		renderSkybox(mouseX, mouseY, partialTicks);
		Gui.drawVerticalLine(SIDEBAR_WIDTH - 2, 0, height, Colors.getColor(22, 255));
		Gui.drawVerticalLine(SIDEBAR_WIDTH - 1, 0, height, Colors.getColor(55, 255));
		Gui.drawRect(0, 0, SIDEBAR_WIDTH, height, Colors.getColor(0, 0, 0, 100));
		drawButtons(mouseX, mouseY);
		accountSlot.drawScreen(mouseX, mouseY, partialTicks);
		fontRendererObj.drawStringWithShadow("Accounts: \2477" + Client.getAccountManager().getArray().length, 3, 13, 0xFFFFFF);
		if (mc.getSession() != null) {
			fontRendererObj.drawStringWithShadow("User: \2477" + mc.getSession().getUsername(), 3, 3, 0xFFFFFF);
		} else {
			fontRendererObj.drawStringWithShadow("User: \2477" + "ERROR", 3, 3, Colors.getColor(200, 22, 22));
		}
		if (!addingAccount) {
			fontRendererObj.drawStringWithShadow(text, 3, 23, Colors.getColor(200, 22, 22));
		}
		if (!isInit) {
			searchBox.drawTextBox();
		}
		if (addingAccount) {
			usernameBox.setVisible(true);
			passwordBox.setVisible(true);
			Gui.drawRect(0, 0, width, height, Colors.getColor(0, 200));
			usernameBox.drawTextBox();
			passwordBox.drawTextBox();
			fontRendererObj.drawStringWithShadow("Username", width / 2 - 100, 63, 0xa0a0a0);
			fontRendererObj.drawStringWithShadow("Password", width / 2 - 100, 104, 0xa0a0a0);
			String strEnter = "Press enter to add account, or escape to cancel";
			fontRendererObj.drawStringWithShadow(strEnter, width / 2 - fontRendererObj.getStringWidth(strEnter) / 2, 143, Colors.getColor(35, 35, 35));
			fontRendererObj.drawStringWithShadow(text, width / 2 - fontRendererObj.getStringWidth(text) / 2, 23, Colors.getColor(200, 22, 22));
		}
	}

	@Override
	public void confirmClicked(boolean flag, int index) {
		super.confirmClicked(flag, index);
		if (deleteMenuOpen) {
			deleteMenuOpen = false;
			if (flag) {
				Client.getAccountManager().remove(Client.getAccountManager().get(index));
			}
			mc.displayGuiScreen(this);
		}
	}

	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
		for (Account account : Client.getAccountManager().getArray()) {
			if (account != null) {
				account.save();
			}
		}
		super.onGuiClosed();
	}

	@Override
	public void updateScreen() {
		searchBox.updateCursorCounter();
		usernameBox.updateCursorCounter();
		passwordBox.updateCursorCounter();
	}

	@Override
	public void mouseClicked(int x, int y, int b) {
		if (!isInit) {
			if (!addingAccount) {
				searchBox.mouseClicked(x, y, b);
			} else {
				usernameBox.mouseClicked(x, y, b);
				passwordBox.mouseClicked(x, y, b);
			}
		}
		try {
			super.mouseClicked(x, y, b);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void keyTyped(char c, int i) {
		if (!addingAccount) {
			searchBox.textboxKeyTyped(c, i);
			if (c == '\r') {
				boolean found = false;
				for (Account account : Client.getAccountManager().getArray()) {
					if (account.getDisplay().equalsIgnoreCase(searchBox.getText())) {
						Session session = Session.loginPassword(account.getUser(), account.getPass(), mc.proxy);
						if (session != null) {
							mc.setSession(session);
							account.setDisplay(session.getUsername());
							account.updateCrypto();
							account.save();
							Client.getAccountManager().setCurrent(account);
							found = true;
							text = "";
						}
						break;
					}
				}
				if (!found) {
					text = "User '" + searchBox.getText() + "' not found";
				}
				searchBox.setText("");
			}
		} else {
			usernameBox.textboxKeyTyped(c, i);
			passwordBox.textboxKeyTyped(c, i);
			if (c == '\t') {
				if (usernameBox.isFocused()) {
					usernameBox.setFocused(false);
					passwordBox.setFocused(true);
				} else {
					usernameBox.setFocused(true);
					passwordBox.setFocused(false);
				}
			}
			if (c == '\r') {
				String user = usernameBox.getText();
				String pass = passwordBox.getText();
				if (user.length() > 4 && pass.length() > 4) {
					Account account = new Account(user, pass);
					Client.getAccountManager().add(account);
					addingAccount = false;
					usernameBox.setText("");
					passwordBox.setText("");
					usernameBox.setVisible(false);
					passwordBox.setVisible(false);
					text = "";
				} else {
					text = "Invalid input.";
				}
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
				addingAccount = false;
				usernameBox.setVisible(false);
				passwordBox.setVisible(false);
				text = "";
			}
		}
	}

	@Override
	public void handleMouseInput() throws IOException {
		super.handleMouseInput();
		if (!addingAccount) {
			accountSlot.handleMouseScrolling();
		}
	}

	private void showDelete() {
		try {
			String accountName = Client.getAccountManager().get(accountSlot.getSelected()).getDisplay();
			String strPrompt = "Are you sure you want to delete the account: " + "\"" + accountName + "\"" + "?";
			String strOptDelete = "Delete";
			String strCancel = "Cancel";
			GuiYesNo guiyesno = new GuiYesNo(this, strPrompt, "", strOptDelete, strCancel, accountSlot.getSelected());
			deleteMenuOpen = true;
			mc.displayGuiScreen(guiyesno);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	class SlotAlt extends GuiSlot {
		public static final int sltHeight = 36;
		private final GuiAccountList aList;
		private int selected;

		public SlotAlt(Minecraft theMinecraft, GuiAccountList aList) {
			super(theMinecraft, aList.width, aList.height, 0, aList.height, sltHeight);
			this.aList = aList;
			this.top = 0;
			this.left = SIDEBAR_WIDTH;
			this.width = width - left;
			this.right = width * 2;
			this.selected = -1;
		}

		@Override
		public int getListWidth() {
			return 220;
		}

		@Override
		public int getSlotHeight() {
			return (int) (slotHeight * 1.2);
		}

		@Override
		public int getScrollBarX() {
			return this.aList.width - 10;
		}

		@Override
		protected int getSize() {
			return Client.getAccountManager().getArray().length;
		}

		@Override
		protected void elementClicked(int i, boolean var2, int idunno, int idunnoeither) {
			this.selected = i;
		}

		@Override
		protected boolean isSelected(int i) {
			return this.selected == i;
		}

		public int getSelected() {
			return this.selected;
		}

		public void setSelected(int i) {
			selected = i;

		}

		@Override
		protected void drawBackground() {
			aList.drawDefaultBackground();
		}

		@Override
		public void drawScreen(int x, int y, float partial) {
			Tessellator var6 = Tessellator.getInstance();
			WorldRenderer var7 = var6.getWorldRenderer();
			this.mouseX = x;
			this.mouseY = y;
			int var4 = this.getScrollBarX();
			int var5 = var4 + 6;
			this.bindAmountScrolled();
			GlStateManager.disableLighting();
			GlStateManager.disableFog();
			int var9 = this.left + this.width / 2 - this.getListWidth() / 2 + 2;
			int var10 = this.top + 4 - (int) this.amountScrolled;

			if (this.hasListHeader) {
				this.drawListHeader(var9, var10, var6);
			}

			this.drawSelectionBox(var9, var10, x, y);
			GlStateManager.disableDepth();
			byte var11 = 4;
			this.overlayBackground(0, this.top, 255, 255);
			this.overlayBackground(this.bottom, this.height, 255, 255);
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(770, 771, 0, 1);
			GlStateManager.disableAlpha();
			GlStateManager.shadeModel(7425);
			GlStateManager.disableTextures();
			var7.startDrawingQuads();
			var7.drawColor(0, 0);
			var7.addVertexWithUV((double) this.left, (double) (this.top + var11), 0.0D, 0.0D, 1.0D);
			var7.addVertexWithUV((double) this.right, (double) (this.top + var11), 0.0D, 1.0D, 1.0D);
			var7.drawColor(0, 255);
			var7.addVertexWithUV((double) this.right, (double) this.top, 0.0D, 1.0D, 0.0D);
			var7.addVertexWithUV((double) this.left, (double) this.top, 0.0D, 0.0D, 0.0D);
			var6.draw();
			var7.startDrawingQuads();
			var7.drawColor(0, 255);
			var7.addVertexWithUV((double) this.left, (double) this.bottom, 0.0D, 0.0D, 1.0D);
			var7.addVertexWithUV((double) this.right, (double) this.bottom, 0.0D, 1.0D, 1.0D);
			var7.drawColor(0, 0);
			var7.addVertexWithUV((double) this.right, (double) (this.bottom - var11), 0.0D, 1.0D, 0.0D);
			var7.addVertexWithUV((double) this.left, (double) (this.bottom - var11), 0.0D, 0.0D, 0.0D);
			var6.draw();
			int var12 = this.func_148135_f();
			if (var12 > 0) {
				int var13 = (this.bottom - this.top) * (this.bottom - this.top) / this.getContentHeight();
				var13 = MathHelper.clamp_int(var13, 32, this.bottom - this.top - 8);
				int var14 = (int) this.amountScrolled * (this.bottom - this.top - var13) / var12 + this.top;

				if (var14 < this.top) {
					var14 = this.top;
				}

				var7.startDrawingQuads();
				var7.drawColor(0, 255);
				var7.addVertexWithUV((double) var4, (double) this.bottom, 0.0D, 0.0D, 1.0D);
				var7.addVertexWithUV((double) var5, (double) this.bottom, 0.0D, 1.0D, 1.0D);
				var7.addVertexWithUV((double) var5, (double) this.top, 0.0D, 1.0D, 0.0D);
				var7.addVertexWithUV((double) var4, (double) this.top, 0.0D, 0.0D, 0.0D);
				var6.draw();
				var7.startDrawingQuads();
				var7.drawColor(8421504, 255);
				var7.addVertexWithUV((double) var4, (double) (var14 + var13), 0.0D, 0.0D, 1.0D);
				var7.addVertexWithUV((double) var5, (double) (var14 + var13), 0.0D, 1.0D, 1.0D);
				var7.addVertexWithUV((double) var5, (double) var14, 0.0D, 1.0D, 0.0D);
				var7.addVertexWithUV((double) var4, (double) var14, 0.0D, 0.0D, 0.0D);
				var6.draw();
				var7.startDrawingQuads();
				var7.drawColor(12632256, 255);
				var7.addVertexWithUV((double) var4, (double) (var14 + var13 - 1), 0.0D, 0.0D, 1.0D);
				var7.addVertexWithUV((double) (var5 - 1), (double) (var14 + var13 - 1), 0.0D, 1.0D, 1.0D);
				var7.addVertexWithUV((double) (var5 - 1), (double) var14, 0.0D, 1.0D, 0.0D);
				var7.addVertexWithUV((double) var4, (double) var14, 0.0D, 0.0D, 0.0D);
				var6.draw();
			}

			this.func_148142_b(x, y);
			GlStateManager.enableTextures();
			GlStateManager.shadeModel(7424);
			GlStateManager.enableAlpha();
			GlStateManager.disableBlend();
		}

		@Override
		protected void drawSlot(int index, int x, int y, int var4, int var6, int var7) {
			try {
				Account account = Client.getAccountManager().get(index);
				if (account == null) {
					drawString(fontRendererObj, "null", x + 22, y + 9, 0x808080);
					return;
				}
				int i = 100;
				if (index == getSelected()) {
					GL11.glColor3f(1, 1, 1);
				} else {
					int hh = y + 31;
					Gui.drawRect(x - 1, y - 1, x + 217, hh + 1, Colors.getColor(0, 55));
					Gui.drawBorderedRect(x - 2, y - 2, x + 217, hh + 1, 1, Colors.getColor(150, 60));
					GL11.glColor3f(0.5f, 0.5f, 0.5f);
				}
				i = ((int) Math.ceil(aList.height / getSlotHeight())) + 1;
				if (y > (0 - getSlotHeight()) && y < (i * getSlotHeight())) {
					String dlStr = account.getDisplay().contains("@") ? Client.author : account.getDisplay();
					ThreadDownloadImageData t = AbstractClientPlayer.getDownloadImageSkin(new ResourceLocation(dlStr + ".png"), dlStr);
					TextureUtil.bindTexture(t.getGlTextureId());
					drawScaledTexturedModalRect(x, y, 32, 32, 32, 32, 1F);
				}
				int ySpread = 9;
				int xBase = 44;
				drawString(fontRendererObj, account.getDisplay(), x + xBase, y + ySpread - 1, 0xFFFFFF);
				drawString(fontRendererObj, account.isPremium() ? "Premium" : "Not Premium", x + xBase, y + ySpread * 2, 0x808080);
				int friends = 0, rivals = 0;
				for (UserStatus status : account.getRelationships().values()) {
					if (status.isFriend()) {
						friends++;
					} else if (status.isRival()) {
						rivals++;
					}
				}
				drawString(fontRendererObj, "Friends: " + friends, x + 155, y + ySpread - 1, 0x808080);
				drawString(fontRendererObj, "Rivals: " + rivals, x + 155, y + ySpread * 2, 0x808080);
			} catch (final Exception error) {
				error.printStackTrace();
			}
		}
	}
}
