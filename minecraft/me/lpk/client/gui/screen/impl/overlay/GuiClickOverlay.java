package me.lpk.client.gui.screen.impl.overlay;

import me.lpk.client.module.data.ModuleData;
import me.lpk.client.util.render.Screen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import me.lpk.client.Client;
import me.lpk.client.gui.clickgui.FrameFactory;
import me.lpk.client.gui.clickgui.component.impl.Frame;
import me.lpk.client.gui.clickgui.renderer.GuiRenderer;
import me.lpk.client.gui.clickgui.renderer.impl.BasicRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;

public class GuiClickOverlay extends GuiScreen {
	private final ArrayList<Frame> frames = new ArrayList<Frame>();
	private final ArrayList<Frame> removeQueue = new ArrayList<Frame>();

	public GuiClickOverlay() {
		//TODO: Theme loading
		//renderer.update(loadedRendererFromSettings);
		// Generate each frame		
		for (ModuleData.Type type : ModuleData.Type.values()) {
			Frame frame = FrameFactory.getFrameFromType(this, type, Client.getGuiTheme());
			if (frame.getChildren().size() > 0) {
				frames.add(frame);
			}
		}
		// Place each frame in the GUI
		int x = 0, y = 0;
		ScaledResolution sr = Screen.getResolution();
		for (Frame frame : frames) {
			if (y + frame.getArea().height >= sr.getScaledHeight()) {
				y = 0;
				x += frame.getArea().width + 2;
			}
			frame.setLocation(x, y);
			y += frame.getArea().height + 2;
		}
	}

	@Override
	public void mouseClicked(int x, int y, int mode) throws IOException {
		// Get the frame at the mouse location
		Frame frame = getFrame(x, y);
		if (frame != null) {
			// If the mouse is over the frame header, notify it of mouse
			// interaction and move it to the top of the list. This will bring
			// the frame in front of the others.
			if (frame.isMouseOverHeader(x, y)) {
				frame.onClick(x, y);
				this.frames.remove(frame);
				this.frames.add(frame);
			} else {
				frame.interact(x, y, true);
			}
		}
		super.mouseClicked(x, y, mode);
	}

	@Override
	protected void mouseReleased(int x, int y, int mode) {
		// Get the frame at the mouse location
		Frame frame = getFrame(x, y);
		if (frame != null) {
			// If the mouse is over the header, release the frame. Otherwise,
			// interact with the frame's content.
			if (frame.isMouseOverHeader(x, y)) {
				frame.onRelease(x, y);
			} else {
				frame.interact(x, y, false);
			}
		}
	}

	/**
	 * Gets a frame based on the mouse's location.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private Frame getFrame(int x, int y) {
		// This is ugly, but it's the only way to get frames that are ON TOP.
		// Otherwise it gets the frames that are in the back. If a reverse
		// arraylist exists, I'd use that instead.
		ArrayList<Frame> tmpFrames = (ArrayList) this.frames.clone();
		Collections.reverse(tmpFrames);
		for (Frame frame : tmpFrames) {
			if (frame.isMouseOver(x, y)) {
				return frame;
			}
		}
		return null;
	}

	/**
	 * Removes queued frames marked for removal then draws the remaining frames.
	 */
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		if (removeQueue.size() > 0) {
			for (Frame removedFrame : removeQueue) {
				frames.remove(removedFrame);
			}
			removeQueue.clear();
		}
		for (Frame frame : frames) {
			frame.movement();
			Client.getGuiTheme().drawFrame(frame, mouseX, mouseY);
		}
	}

	/**
	 * Adds a frame to the remove queue.
	 * 
	 * @param frame
	 */
	public void remove(Frame frame) {
		removeQueue.add(frame);
	}
}
