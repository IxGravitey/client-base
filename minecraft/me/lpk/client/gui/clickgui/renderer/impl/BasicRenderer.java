package me.lpk.client.gui.clickgui.renderer.impl;

import me.lpk.client.gui.clickgui.component.Component;
import me.lpk.client.gui.clickgui.component.impl.Frame;
import me.lpk.client.gui.clickgui.component.impl.LinkedButton;
import me.lpk.client.gui.clickgui.component.impl.Panel;
import me.lpk.client.gui.clickgui.component.impl.info.CommandInfo;
import me.lpk.client.gui.clickgui.component.impl.info.ModuleInfo;
import me.lpk.client.gui.clickgui.component.impl.tabs.Tab;
import me.lpk.client.gui.clickgui.component.impl.tabs.TabbedPanel;
import me.lpk.client.gui.clickgui.renderer.GuiRenderer;
import me.lpk.client.module.Module;
import me.lpk.client.util.render.Colors;
import me.lpk.client.util.render.Screen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;

public class BasicRenderer extends GuiRenderer {
	@Override
	public void setup() {
		headerHeight = 20;
		headerWidth = 5;
		objectMarginHorizontal = 3;
		objectMarginVertical = 1;
		objectPaddingVertical = 0;
		objectPaddingHorizontal = 3;
		objectHeight = 15;
	}

	@Override
	public void drawFrame(Frame frame, int mouseX, int mouseY) {
		double lineWidth = 0.5D;
		double frameX = frame.getLocation().x - lineWidth;
		double frameY = frame.getLocation().y - lineWidth;
		double frameWidth = frame.getLocation().x + frame.getArea().width;
		double frameHeight = frame.getLocation().y + frame.getArea().height;
		double headerHeight = frame.getLocation().y + this.headerHeight - this.objectMarginVertical * 3;
		int titleOffset = 6;
		// Draw the frame
		Gui.drawRect(frameX, frameY, frameWidth, frameHeight + 1, Colors.getColor(55));
		Gui.drawBorderedRect(frameX, frameY, frameWidth, frameHeight + 1, 1, Colors.getColor(20, 25, 22));
		Gui.drawRect(frameX, frameY, frameWidth, headerHeight, Colors.getColor(80, 112, 117));
		Gui.drawBorderedRect(frameX, frameY, frameWidth, headerHeight, 1, Colors.getColor(80, 112, 118));
		Gui.drawBorderedRect(frameX, frameY, frameWidth, headerHeight, 1, Colors.getColor(40, 52, 57));
		// Draw the text on the frame
		mc.fontRendererObj.drawString(frame.getTitle(), (int) frameX + 1 + titleOffset, (int) frameY + 1 + titleOffset, Colors.getColor(30, 56, 58));
		mc.fontRendererObj.drawString(frame.getTitle(), (int) frameX + titleOffset, (int) frameY + titleOffset, -1);
		// Iterate through children and draw those
		for (Component component : frame.getChildren()) {
			double x = component.getLocation().x;
			double y = component.getLocation().y;
			if (component instanceof LinkedButton) {
				LinkedButton btn = (LinkedButton) component;
				Module m = (Module) btn.getLinked().get(0);
				int color = m.isEnabled() ? Colors.getColor(80, 112, 118) : Colors.getColor(55, 66, 70);
				int txtColor = m.isEnabled() ? Colors.getColor(255) : Colors.getColor(65, 83, 86);
				int txtColor2 = m.isEnabled() ? Colors.getColor(55, 66, 70) : Colors.getColor(25, 36, 39);
				Gui.drawRect(x, y, x + component.getArea().width, y + component.getArea().height, color);
				Gui.drawBorderedRect(x, y, x + component.getArea().width, y + component.getArea().height, 1, Colors.getColor(35, 43, 47));
				int nameWidth = mc.fontRendererObj.getStringWidth(m.getName());
				mc.fontRendererObj.drawString(m.getName(), (int) (1 + x + component.getArea().width / 2 - nameWidth / 2), (int) y + 5, txtColor2);
				mc.fontRendererObj.drawString(m.getName(), (int) (x + component.getArea().width / 2 - nameWidth / 2), (int) y + 4, txtColor);
			}
		}
	}

	@Override
	public void drawTabbedPanel(TabbedPanel tabbed, int mouseX, int mouseY) {
		try {
			tabbed.setWidth(Screen.getResolution().getScaledWidth());
			// Draw the BG color of the tabbed panel
			Gui.drawRect(tabbed.getLocation().x, tabbed.getLocation().y, tabbed.getArea().width, tabbed.getArea().height, Colors.getColor(30, 37, 40));
			// Draw the tabs
			for (Tab tab : tabbed.getTabs()) {
				int tx = tab.getLocation().x;
				int ty = tab.getLocation().y;
				int color = tab.isSelected() ? Colors.getColor(80, 112, 117) : Colors.getColor(40, 66, 68);
				Gui.drawRect(tx, ty, tx + tab.getArea().width, tab.getArea().height, color);
				Gui.drawBorderedRect(tx, ty, tx + tab.getArea().width, tab.getArea().height, 1, Colors.getColor(0));
				int txtx = tab.getLocation().x + tab.getArea().width / 2 - mc.fontRendererObj.getStringWidth(tab.getTitle()) / 2;
				mc.fontRendererObj.drawString(tab.getTitle(), txtx, tab.getLocation().y + 6, -1);
			}
			// Draw the panel's expected dimensions
			Gui.drawRect(0, tabbed.getTabHeight(), tabbed.getArea().width, tabbed.getArea().height, Colors.getColor(40, 52, 57));
			Gui.drawBorderedRect(0, tabbed.getTabHeight(), tabbed.getArea().width, tabbed.getArea().height, 1, Colors.getColor(0));
			Panel panel = tabbed.getShownPanel();
			if (panel != null) {
				// Update the width if the user changes their screen size (which
				// will change the TabbedPanel area)
				panel.setWidth(tabbed.getArea().width);
				int tx = panel.getLocation().x;
				int ty = panel.getLocation().y;
				// Draw the panel's children (The command / module buttons)
				for (Component c : panel.getChildren()) {
					boolean over = c.isMouseOver(mouseX, mouseY);
					int cx = c.getLocation().x;
					int cy = c.getLocation().y;
					int txtCol = over ? Colors.getColor(255) : Colors.getColor(85, 113, 126);
					int txtx = -1;
					String name = "ERROR";
					if (c instanceof ModuleInfo) {
						ModuleInfo mi = (ModuleInfo) c;
						name = mi.getModule().getName();
						txtx = cx + c.getArea().width / 2 - mc.fontRendererObj.getStringWidth(name) / 2;
					} else if (c instanceof CommandInfo) {
						CommandInfo ci = (CommandInfo) c;
						name = ci.getCommand().getName();
						txtx = cx + c.getArea().width / 2 - mc.fontRendererObj.getStringWidth(name) / 2;
					}
					int color = over ? Colors.getColor(80, 112, 118) : Colors.getColor(55, 66, 70);
					Gui.drawRect(cx, cy, cx + c.getArea().width, cy + c.getArea().height, color);
					Gui.drawBorderedRect(cx, cy, cx + c.getArea().width, cy + c.getArea().height, 1, Colors.getColor(35, 43, 47));
					mc.fontRendererObj.drawString(name, txtx, cy + 4, txtCol);
				}
			} else {
				// If no tab is selected tell the user to chose one of the tabs
				int tx = (int) (tabbed.getArea().getWidth() / 2 - mc.fontRendererObj.getStringWidth("Select a tab") / 2);
				mc.fontRendererObj.drawString("Select a tab", tx, (int) (tabbed.getArea().getHeight() / 2), -1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
