package me.lpk.client.gui.clickgui.component;

import java.awt.Point;
import java.awt.Rectangle;

import com.google.gson.annotations.Expose;

public abstract class Component {
	private final Component parent;
	private Rectangle area;
	@Expose
	private int x;
	@Expose
	private int y;

	public Component(Rectangle area, Component parent) {
		this.area = area;
		this.x = area.x;
		this.y = area.y;
		this.parent = parent;
	}

	public Rectangle getArea() {
		return this.area;
	}

	/**
	 * Returns the x,y location of the component. Accounts for if the component
	 * is a child of another.
	 * 
	 * @return
	 */
	public Point getLocation() {
		if (this.parent != null) {
			return new Point(this.parent.getLocation().x + this.x, this.parent.getLocation().y + this.y);
		}
		return new Point(this.x, this.y);
	}

	protected Component getParent() {
		return this.parent;
	}

	public boolean isMouseOver(int x, int y, int buffer) {
		return (x > getLocation().x - buffer) && (x < getLocation().x + getArea().width + buffer) && (y > getLocation().y - buffer) && (y < getLocation().y + getArea().height + buffer);
	}

	public boolean isMouseOver(int x, int y) {
		return isMouseOver(x, y, 0);
	}

	public abstract void onClick(int x, int y);

	public abstract void onRelease(int x, int y);

	protected void setArea(Rectangle area) {
		this.area = area;
	}

	public void setHeight(int h) {
		this.area.height = h;
	}

	public void setWidth(int w) {
		this.area.width = w;
	}

	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setLocation(Point location) {
		setLocation(location.x, location.y);
	}
}