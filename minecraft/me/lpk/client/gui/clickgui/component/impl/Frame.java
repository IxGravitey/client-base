package me.lpk.client.gui.clickgui.component.impl;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;

import me.lpk.client.Client;
import me.lpk.client.gui.clickgui.component.Component;
import me.lpk.client.gui.screen.impl.overlay.GuiClickOverlay;
import me.lpk.client.util.render.Screen;
import net.minecraft.client.Minecraft;

public class Frame extends Panel {
	private final GuiClickOverlay screen;
	private boolean closeable, isDragging;
	private final String title;
	private int mouseX = 0;
	private int mouseY = 0;

	public Frame(GuiClickOverlay screen, Rectangle area, String title) {
		super(area, null);
		this.screen = screen;
		this.title = title;
		this.closeable = false;
	}

	public Frame(GuiClickOverlay gui, Rectangle area, String title, boolean closeable) {
		this(gui, area, title);
		this.closeable = closeable;
	}

	@Override
	public void onClick(int x, int y) {
		setDragging(true);
		this.mouseX = (getLocation().x - x);
		this.mouseY = (getLocation().y - y);
		if ((this.closeable) && (Mouse.isButtonDown(2))) {
			this.screen.remove(this);
		}
	}

	@Override
	public void onRelease(int x, int y) {
		setDragging(false);
	}

	/**
	 * Interact with the children of the frame.
	 * 
	 * @param x
	 * @param y
	 * @param isDown
	 */
	public void interact(int x, int y, boolean isDown) {
		for (Component c : getChildren()) {
			if (c.isMouseOver(x, y)) {
				if (isDown) {
					c.onClick(x, y);
				} else {
					c.onRelease(x, y);
				}
			}
		}
	}

	/**
	 * Handle dragging.
	 */
	public void movement() {
		if (isDragging) {
			setLocation(new Point(Screen.getMouseX() + this.mouseX, Screen.getMouseY() + this.mouseY));
		}
	}

	/**
	 * Checks if the mouse is over the draggable area of the frame.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isMouseOverHeader(int x, int y) {
		return (x > getLocation().x) && (x < getLocation().x + getArea().width) && (y > getLocation().y) && (y < getLocation().y + Client.getGuiTheme().getHeaderHeight());
	}

	public String getTitle() {
		return this.title;
	}

	public void setDragging(boolean dragging) {
		isDragging = dragging;
	}

	public boolean getDragging() {
		return isDragging;
	}
}