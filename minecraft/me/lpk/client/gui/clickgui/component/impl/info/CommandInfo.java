package me.lpk.client.gui.clickgui.component.impl.info;

import java.awt.Rectangle;

import me.lpk.client.command.Command;
import me.lpk.client.gui.clickgui.component.Component;

public class CommandInfo extends Component {
	private final Command command;

	public CommandInfo(Command command, Rectangle area, Component parent) {
		super(area, parent);
		this.command = command;
	}

	@Override
	public void onClick(int x, int y) {}

	@Override
	public void onRelease(int x, int y) {}

	public Command getCommand() {
		return command;
	}
}
