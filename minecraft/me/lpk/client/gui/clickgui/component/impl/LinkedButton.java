package me.lpk.client.gui.clickgui.component.impl;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import me.lpk.client.gui.clickgui.GUILinked;
import me.lpk.client.gui.clickgui.component.Component;

public class LinkedButton extends Component {
	private final List<GUILinked> linked;

	public LinkedButton(Rectangle area, Component parent, GUILinked linked) {
		super(area, parent);
		this.linked = new ArrayList<GUILinked>();
		this.linked.add(linked);
	}

	public LinkedButton(Rectangle area, Component parent, List<GUILinked> linked) {
		super(area, parent);
		this.linked = linked;
	}

	@Override
	public void onClick(int x, int y) {
		for (GUILinked linked : this.linked) {
			linked.onItemClick();
		}
	}

	@Override
	public void onRelease(int x, int y) {
		for (GUILinked linked : this.linked) {
			linked.onItemRelease();
		}
	}

	public List<GUILinked> getLinked() {
		return linked;
	}

}
