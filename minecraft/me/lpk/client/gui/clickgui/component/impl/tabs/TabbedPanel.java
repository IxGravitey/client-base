package me.lpk.client.gui.clickgui.component.impl.tabs;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import me.lpk.client.command.Command;
import me.lpk.client.gui.clickgui.component.Component;
import me.lpk.client.gui.clickgui.component.impl.Panel;
import me.lpk.client.gui.clickgui.component.impl.info.CommandInfo;
import me.lpk.client.gui.clickgui.component.impl.info.ModuleInfo;
import me.lpk.client.module.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;

public class TabbedPanel extends Component {
	private final HashMap<Integer, Panel> panels = new HashMap<Integer, Panel>();
	private final HashMap<Integer, Tab> tabs = new HashMap<Integer, Tab>();
	private final ArrayList<String> displayText = new ArrayList<String>();
	private Panel shown;

	public TabbedPanel(Rectangle area, Component parent) {
		super(area, parent);
	}

	@Override
	public void onClick(int x, int y) {
		for (int i = 0; i < tabs.size(); i++) {
			Tab tab = tabs.get(i);
			if (tab.isMouseOver(x, y)) {
				displayText.clear();
				for (Tab other : tabs.values()) {
					other.setSelected(false);
				}
				tab.setSelected(true);
				shown = panels.get(i);
			}
		}
		if (shown != null) {
			for (Component component : shown.getChildren()) {
				if (!component.isMouseOver(x, y)) {
					continue;
				}
				displayText.clear();
				if (component instanceof ModuleInfo) {
					ModuleInfo mi = (ModuleInfo) component;
					Module m = mi.getModule();
					displayText.add(EnumChatFormatting.UNDERLINE + "Name" + EnumChatFormatting.RESET + ": " + m.getName());
					displayText.add(EnumChatFormatting.UNDERLINE + "Description" + EnumChatFormatting.RESET + ": " + m.getDescription());
					String mask = m.getKeybind().getMask().name();
					displayText.add(EnumChatFormatting.UNDERLINE + "Key" + EnumChatFormatting.RESET + ": " + (mask.toLowerCase().contains("none") ? "" : mask + " + ") + m.getKeybind().getKeyStr());
				} else if (component instanceof CommandInfo) {
					CommandInfo mi = (CommandInfo) component;
					Command m = mi.getCommand();
					displayText.add(EnumChatFormatting.UNDERLINE + "Name" + EnumChatFormatting.RESET + ": " + m.getName());
					displayText.add(EnumChatFormatting.UNDERLINE + "Usage" + EnumChatFormatting.RESET + ": " + m.getUsage());
				}
			}
		}
	}

	@Override
	public void onRelease(int x, int y) {

	}

	public void addTab(String text, Panel panel) {
		int x = getTabX(tabs.size());
		int wid = Minecraft.getMinecraft().fontRendererObj.getStringWidth(text) + 10;
		Tab tab = new Tab(text, new Rectangle(x, 0, wid, getTabHeight()), this);
		// tab.setHeight(getTabHeight());
		// tab.setWidth(50);
		// tab.setLocation(getTabX(tabs.size()), 0);
		int i = tabs.size();
		tabs.put(i, tab);
		panels.put(i, panel);
	}

	private int getTabX(int index) {
		if (index == 0) {
			return 0;
		}
		int x = 0;
		for (int i = 0; i < index; i++) {
			x += tabs.get(i).getArea().width + 5;
		}
		return x;
	}

	public int getTabHeight() {
		return 20;
	}

	public Panel getShownPanel() {
		return shown;
	}

	public ArrayList<String> getDisplayText() {
		return displayText;
	}

	public Collection<Tab> getTabs() {
		return tabs.values();
	}
}
