package me.lpk.client.event.impl;

import me.lpk.client.event.Event;
import net.minecraft.network.Packet;

public class EventMotion extends Event {
	private boolean isPre;

	public void fire(boolean isPre) {
		this.isPre = isPre;
		super.fire();
	}

	public boolean isPre() {
		return isPre;
	}

	public boolean isPost() {
		return !isPre;
	}
}
