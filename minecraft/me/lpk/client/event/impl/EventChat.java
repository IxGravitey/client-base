package me.lpk.client.event.impl;

import me.lpk.client.Client;
import me.lpk.client.event.Event;

public class EventChat extends Event {
	private String text;

	public void fire(String text) {
		this.text = text;
		super.fire();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
