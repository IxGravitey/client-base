package me.lpk.client.event;

public interface EventListener<E extends Event> {
	void onEvent(E event);
}
